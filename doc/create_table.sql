drop table if exists CATEGORIE;
drop table if exists POI;
drop table if exists android_metadata;

PRAGMA foreign_keys = ON;

create table CATEGORIE
(
	ID_CAT		integer primary key,
	NOM_CAT		varchar(48) not null,
	DESCRIPTION	varchar(1024)
);

create table POI
(
	ID_POI		integer primary key,
	LATITUDE	int not null,
	LONGITUDE	int not null,
	NOM_POI		varchar(48) not null,
	DESCRIPTION	varchar(1024),
	ID_CAT		integer,
	foreign key(ID_CAT) references CATEGORIE(ID_CAT)
);

create table android_metadata (locale text default 'en_US');
insert into android_metadata values ('fr_FR');
