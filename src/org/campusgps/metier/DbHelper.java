package org.campusgps.metier;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {
	
	private static String db_path;
	private final static String DB_NAME = "data_campusgps.db";
	private final static int VERSION = 1;
	
	private static DbHelper instance;
	

	private DbHelper(Context context) {
		super(context, DB_NAME, null, VERSION);
		if(db_path == null)
			db_path = "/data/data/"+context.getApplicationContext().getPackageName()+"/databases/";
		if(!checkDatabase()) {
			copyDatabase(context);
		}
	}
	
	public static DbHelper getInstance(Context context) {
		if(instance == null)
			instance = new DbHelper(context);
		
		return instance;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}
	/**
	 * Vérifie si la bdd existe déjà
	 * @return true si elle existe, false sinon
	 */
	private boolean checkDatabase() {
		SQLiteDatabase checkDb = null;

		try {
			//Vérification présence base
			checkDb = SQLiteDatabase.openDatabase(db_path+DB_NAME, null, SQLiteDatabase.OPEN_READONLY);
		}
		catch (SQLException e) {
			//La base de donnée n'existe pas
		}
		finally {
			if(checkDb != null)
				checkDb.close();
		}
		
		return checkDb != null;
	}

	private void copyDatabase(Context context) {
		//Crée une base vide que l'on peut écraser par la suite
		getReadableDatabase().close();

		try {
			InputStream input = context.getAssets().open(DB_NAME);
			OutputStream output = new FileOutputStream(db_path+DB_NAME);
			
			byte[] buffer = new byte[1024];
			int length;
			while ((length = input.read(buffer))>0){
	    		output.write(buffer, 0, length);
	    	}
			
			output.flush();
			output.close();
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
