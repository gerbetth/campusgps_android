package org.campusgps.metier;

public class Categorie {

	private final int id;
	private final String nom;
	private final String description;
	
	public Categorie(int id, String nom, String description) {
		this.nom = nom;
		this.description = description;
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public String getDescription() {
		return description;
	}
}