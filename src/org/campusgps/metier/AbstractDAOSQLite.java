package org.campusgps.metier;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public abstract class AbstractDAOSQLite<T> implements DAO<T> {
	private SQLiteDatabase db;
	
	public AbstractDAOSQLite(Context context) {
		db = DbHelper.getInstance(context).getReadableDatabase();
	}

	protected SQLiteDatabase getDb() {
		return db;
	}
}
