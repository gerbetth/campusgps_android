package org.campusgps.metier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.database.sqlite.SQLiteDatabase;


public class WebService {
	private static final String[] TABLE_MAJ = {"POI_LOG", "CATEGORIE_LOG"};
	
	private static final String VERSION = "version.php";
	private static final String GET = "get.php";
	
	private String urlWebService;
	private SQLiteDatabase db;

	public WebService(DbHelper dbHelper, String urlWebService) {
		this.db = dbHelper.getReadableDatabase();
		this.urlWebService = urlWebService;
	}
	
	public void pull() throws IOException, JSONException, Exception {
		int dbVersion = db.getVersion();
		int webServiceVersion = getWebServiceVersion();
		if(webServiceVersion > dbVersion) {
			HttpPost httpPost = new HttpPost(urlWebService+GET);
			HttpClient httpClient = new DefaultHttpClient();
			
			List<NameValuePair> postParam = new ArrayList<NameValuePair>();
			postParam.add(new BasicNameValuePair("type", "pc"));
			postParam.add(new BasicNameValuePair("version", String.valueOf(dbVersion)));
			httpPost.setEntity(new UrlEncodedFormEntity(postParam));
			
			HttpResponse httpResponse = httpClient.execute(httpPost);
			
			JSONObject response = new JSONObject(EntityUtils.toString(httpResponse.getEntity()));
			
			if(response.has("errors"))
				throw new Exception("Erreur requete");
			
			parseAndUpdateDatabase(response);
			db.setVersion(webServiceVersion);
		}
	}
	
	
	private int getWebServiceVersion() throws IOException, JSONException {
		HttpPost httpPost = new HttpPost(urlWebService+VERSION);
		HttpClient httpClient = new DefaultHttpClient();
		HttpResponse httpResponse = httpClient.execute(httpPost);

		JSONObject jObject = new JSONObject(EntityUtils.toString(httpResponse.getEntity()));

		return Integer.valueOf((String) jObject.get("VERSION"));
	}
	
	private void parseAndUpdateDatabase(JSONObject response) throws JSONException {
		JSONArray table;
		db.beginTransaction();
		
		for(String tab : TABLE_MAJ) {
			table = response.getJSONArray(tab);
			JSONObject row;
			for(int i = 0; i < table.length(); i++) {
				row = table.getJSONObject(i);
				
				if(tab.equals("CATEGORIE_LOG")) {
					String sqlAction = row.getString("SQL_ACTION");
					int idCat = row.getInt("ID_CAT");
					String nomCat = row.getString("NOM_CAT_NEW");
					String descCat = row.getString("DESCRIPTION_NEW");
					
					if(sqlAction.equals("INSERT")) {
						db.execSQL("INSERT INTO CATEGORIE VALUES("+idCat+",'"+nomCat+"','"+descCat+"')");
					}
					else if(sqlAction.equals("UPDATE")) {
						db.execSQL("UPDATE CATEGORIE SET NOM_CAT = '"+nomCat+"', DESCRIPTION = '"+descCat+"' WHERE ID_POI = "+idCat);
					}
					else {
						db.execSQL("DELETE FROM CATEGORIE WHERE ID_CAT = "+idCat);
					}
				}
				else if (tab.equals("POI_LOG")) {
					String sqlAction = row.getString("SQL_ACTION");
					int idPoi = row.getInt("ID_POI");
					int latitude = row.getInt("LATITUDE_NEW");
					int longitude = row.getInt("LONGITUDE_NEW");
					String nomPoi = row.getString("NOM_POI_NEW");
					String descPoi = row.getString("DESCRIPTION_NEW");
					int idCat = row.getInt("ID_CAT_NEW");
					
					if(sqlAction.equals("INSERT")) {
						db.execSQL("INSERT INTO POI VALUES("+idPoi+","+latitude+","+longitude+",'"+nomPoi+"','"+descPoi+"',"+idCat+")");
					}
					else if(sqlAction.equals("UPDATE")) {
						db.execSQL("UPDATE POI SET LATITUDE = "+latitude+", LONGITUDE = "+longitude+", NOM_POI = '"+nomPoi+"', DESCRIPTION = '"+descPoi+"', ID_CAT = "+idCat+" WHERE ID_POI = "+idPoi);
					}
					else {
						db.execSQL("DELETE FROM POI WHERE ID_CAT = "+idCat);
					}
				}
				
			}
		}
		db.setTransactionSuccessful();
	}
	
}
