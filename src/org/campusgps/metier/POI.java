package org.campusgps.metier;

import org.mapsforge.core.GeoPoint;


public class POI extends GeoPoint {

	private static final long serialVersionUID = 1L;
	
	private final int id;
	private final String nom;
	private final String description;
	
	private final Categorie categorie;
	
	
	POI(int id, int latitude, int longitude, String nom, String description, Categorie categorie) {
		super(latitude, longitude);
		this.nom = nom;
		this.description = description;
		this.categorie = categorie;
		this.id = id;
	}
		
	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public String getDescription() {
		return description;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if(getClass() == obj.getClass()) {
			POI other = (POI) obj;
			
			if (this.id == other.id)
					return true;
			else if(this.categorie != null && other.categorie != null)
				return this.categorie.getId() == other.categorie.getId() 
						&& this.nom.equals(other.nom) && this.description.equals(other.description);
		}
		
		return false;
	}	
	
}
