package org.campusgps.metier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;

public class MapFileHelper {
	private final static String MAP_NAME = "campusgps.map";

	public static String getPath(Context context) {
		String path = context.getCacheDir().getPath()+'/'+MAP_NAME;
		File mapFile = new File(path);
		
		if(!mapFile.exists())
			copyMapFile(context, mapFile);
		
		return path;
	}
	
	
	private static void copyMapFile(Context context, File map) {

		try {
			OutputStream output = new FileOutputStream(map);
			InputStream input = context.getAssets().open(MAP_NAME);

			byte[] buffer = new byte[1024];
			int length;
			while ((length = input.read(buffer))>0){
				output.write(buffer, 0, length);
			}

			output.flush();
			output.close();
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
}
