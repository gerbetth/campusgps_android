package org.campusgps.metier;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;

public class CategorieDAOSQLite extends AbstractDAOSQLite<Categorie>{
	
	private final static String BDD_TABLE = "CATEGORIE";
	private final static String[] BDD_COLUMNS = new String[] {"ID_CAT", "NOM_CAT", "DESCRIPTION"};
	

	public CategorieDAOSQLite(Context context) {
		super(context);
	}

	@Override
	public Categorie find(int id) {
		Categorie res = null;
		Cursor c = getDb().query(BDD_TABLE, BDD_COLUMNS, "ID_CAT = ?", new String[] { String.valueOf(id) }, null, null, null);;
		if(c.moveToFirst())
			res = cursorToCategorie(c);
			
		c.close();
		
		return res;
	}

	@Override
	public List<Categorie> getAll() {

		Cursor c = getDb().query(BDD_TABLE, BDD_COLUMNS, null, null, null, null, null);
		
		ArrayList<Categorie> res = new ArrayList<Categorie>();

		while(c.moveToNext()) {
			Categorie cat = cursorToCategorie(c);
			
			res.add(cat);
		}
		
		c.close();
		return res;
	}
	
	@Override
	public List<Categorie> search(String word) {
		Cursor c = getDb().query(BDD_TABLE, BDD_COLUMNS, "NOM_CAT Like ?", new String[] { '%' + word + '%' }, null, null, null);
		
		ArrayList<Categorie> res = new ArrayList<Categorie>();

		while(c.moveToNext()) {
			Categorie cat = cursorToCategorie(c);
			
			res.add(cat);
		}
		
		c.close();
		return res;
	}

	private Categorie cursorToCategorie(Cursor c) {
		return new Categorie(c.getInt(0), c.getString(1), c.getString(2));
	}
}
