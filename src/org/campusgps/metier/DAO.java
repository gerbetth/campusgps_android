package org.campusgps.metier;

import java.util.List;

public interface DAO<T> {
	T find(int id);
	List<T> getAll();
	List<T> search(String word); 

}