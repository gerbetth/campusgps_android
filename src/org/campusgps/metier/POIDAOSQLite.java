package org.campusgps.metier;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;

public class POIDAOSQLite extends AbstractDAOSQLite<POI>{
	
	private final static String BDD_TABLE = "POI";
	private final static String[] BDD_COLUMNS = new String[] {"ID_POI", "LATITUDE", "LONGITUDE", "NOM_POI", "DESCRIPTION", "ID_CAT"};
	
	private Context context;

	public POIDAOSQLite(Context context) {
		super(context);
		this.context = context;
	}

	@Override
	public POI find(int id) {
		Cursor c = getDb().query(BDD_TABLE, BDD_COLUMNS, "ID_POI = ?", new String[] { String.valueOf(id) }, null, null, null);
		POI res = null;
		if(c.moveToFirst())
			res = cursorToPOI(c);
		
		c.close();
		return res;
	}

	@Override
	public List<POI> getAll() {
		Cursor c = getDb().query(BDD_TABLE, BDD_COLUMNS, null, null, null, null, null);
		
		ArrayList<POI> res = new ArrayList<POI>();

		while(c.moveToNext()) {
			res.add(cursorToPOI(c));
		}
		
		c.close();
		return res;
	}
	
	public List<POI> getAll(Categorie cat) {
		Cursor c = getDb().query(BDD_TABLE, BDD_COLUMNS, "ID_CAT = ?", new String[] { String.valueOf(cat.getId()) }, null, null, null);
		
		ArrayList<POI> res = new ArrayList<POI>();

		while(c.moveToNext()) {
			res.add(cursorToPOI(c));
		}
		
		c.close();
		return res;
	}
	
	@Override
	public List<POI> search(String word) {
		Cursor c = getDb().query(BDD_TABLE, BDD_COLUMNS, "NOM_POI Like ?", new String[] { '%' + word + '%' }, null, null, null);
		
		ArrayList<POI> res = new ArrayList<POI>();
		
		while(c.moveToNext()) {
			res.add(cursorToPOI(c));
		}
		
		c.close();
		return res;
	}
	
	private POI cursorToPOI(Cursor c) {
		return new POI(c.getInt(0),
				c.getInt(1), c.getInt(2), c.getString(3),
				c.getString(4), new CategorieDAOSQLite(context).find(c.getInt(5)));
	}

}
