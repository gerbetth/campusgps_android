package org.campusgps.android;

import android.os.Bundle;
import android.support.v4.app.SherlockPreferenceActivity;

public class Option extends SherlockPreferenceActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.pref);
		getSupportActionBar().setDisplayShowHomeEnabled(false);
	}
}
