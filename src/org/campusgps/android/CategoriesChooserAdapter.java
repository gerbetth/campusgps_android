package org.campusgps.android;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

public class CategoriesChooserAdapter extends ArrayAdapter<CategorieCheckbox> {
	
	private List<CategorieCheckbox> categorieCheckboxes; 

	public CategoriesChooserAdapter(Context context, List<CategorieCheckbox> list) {
		super(context, R.layout.rowcategorieschooser, list);
		categorieCheckboxes = list;
	}
	
	
	private static class ViewHolder {
		protected TextView text;
		protected CheckBox checkbox;
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		
		if(convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.rowcategorieschooser, null);
			holder = new ViewHolder();
			holder.text = (TextView) convertView.findViewById(R.id.label);
			holder.checkbox = (CheckBox) convertView.findViewById(R.id.check);
			holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					CategorieCheckbox catCb = (CategorieCheckbox) holder.checkbox.getTag();
					catCb.setSelected(buttonView.isChecked());
					
				}
			});
			
			holder.checkbox.setTag(categorieCheckboxes.get(position));
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		CategorieCheckbox cat = categorieCheckboxes.get(position);
		
		holder.checkbox.setChecked(cat.isSelected());
		holder.text.setText(cat.getCategorie().getNom());
		
		return convertView;
		
	}


	public List<CategorieCheckbox> getCategorieCheckboxes() {
		return categorieCheckboxes;
	}
	
}
