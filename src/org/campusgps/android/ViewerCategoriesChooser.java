package org.campusgps.android;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class ViewerCategoriesChooser extends FragmentActivity {
	private FragmentCategoriesChooser categoriesChooser;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.listfragment);
		categoriesChooser = (FragmentCategoriesChooser) getSupportFragmentManager().findFragmentById(R.id.list_fragment);
		
		getSupportActionBar().setTitle(getText(R.string.select_cat));
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		
		Intent intent = getIntent();
		List<Integer> listIDCategorie = intent.getIntegerArrayListExtra("categoriesSelected");
		categoriesChooser.enableItems(listIDCategorie);
	}

	@Override
	public void finish() {
		Intent intent = getIntent();
		intent.putIntegerArrayListExtra("categoriesSelected", categoriesChooser.getEnabledItems());
		setResult(RESULT_OK, intent);
		super.finish();
	}
	
	

}
