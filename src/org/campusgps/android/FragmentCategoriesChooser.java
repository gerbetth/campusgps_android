package org.campusgps.android;


import java.util.ArrayList;
import java.util.List;

import org.campusgps.metier.Categorie;
import org.campusgps.metier.CategorieDAOSQLite;

import android.os.Bundle;
import android.support.v4.app.ListFragment;

public class FragmentCategoriesChooser extends ListFragment {
	CategoriesChooserAdapter categoriesChooserAdapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		List<CategorieCheckbox> listCatCheckb = new ArrayList<CategorieCheckbox>();
		CategorieDAOSQLite CatDAO = new CategorieDAOSQLite(getActivity());
		for(Categorie cat : CatDAO.getAll()) {
			listCatCheckb.add(new CategorieCheckbox(cat));
		}
		
		categoriesChooserAdapter = new CategoriesChooserAdapter(getActivity(), listCatCheckb);
		setListAdapter(categoriesChooserAdapter);
	}
	
	public void enableItems(List<Integer> items) {
		for(CategorieCheckbox catcb : categoriesChooserAdapter.getCategorieCheckboxes()) {
			catcb.setSelected(items.contains(catcb.getCategorie().getId()));
		}
		
		categoriesChooserAdapter.notifyDataSetChanged();
	}
	
	public ArrayList<Integer> getEnabledItems() {
		ArrayList<Integer> res = new ArrayList<Integer>();
		for(CategorieCheckbox catcb : categoriesChooserAdapter.getCategorieCheckboxes()) {
			if(catcb.isSelected())
				res.add(catcb.getCategorie().getId());
		}
		
		return res;
	}

}
