package org.campusgps.android;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.campusgps.metier.Categorie;
import org.campusgps.metier.CategorieDAOSQLite;
import org.campusgps.metier.MapFileHelper;
import org.campusgps.metier.POI;
import org.campusgps.metier.POIDAOSQLite;
import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.overlay.ArrayItemizedOverlay;
import org.mapsforge.android.maps.overlay.OverlayItem;
import org.mapsforge.core.GeoPoint;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.v4.app.FragmentMapActivity;
import android.support.v4.view.Menu;
import android.support.v4.view.MenuItem;
import android.view.KeyEvent;


/**
 * Accueil de l'application
 *
 */
public class BasicMapViewer extends FragmentMapActivity implements LocationListener {
	private final static int CATEGORIE_SELECTION = 1000;
	
	private boolean searchPossible = true;
	private MapView mapView;
	private LocationManager locationManager;
	private Location lastKnowLocation;
	private ArrayItemizedOverlay gpsItemizedOverlay;
	private OverlayItem gpsOverlayItem;
	private List<Categorie> categoriesSelected;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_simple);
		
		/*
		 * Affichage fond de carte
		 */
		mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);
		mapView.setMapFile(MapFileHelper.getPath(this));
		mapView.getController().setCenter(mapView.getMapDatabase().getMapFileInfo().mapCenter);

		/*
		 * Récupération des services nécessaires
		 */
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
		/*
		 * Préparation objet pour point GPS
		 */
		gpsOverlayItem = new OverlayItem();
		
		/*
		 * Gestion de l'action bar
		 */
		getSupportActionBar().setTitle("CampusGPS");
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		
		/*
		 * Affichage des POIs
		 */
		Intent intent = getIntent();
		POIDAOSQLite POIDb = new POIDAOSQLite(this);
		CategorieDAOSQLite CatDb = new CategorieDAOSQLite(this);
		//Liste des POIs à afficher
		List<POI> listPOI = null;
		
		//On vérifie si l'utilisateur a fait une recherche
		if(Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String query = intent.getStringExtra(SearchManager.QUERY);
			
			/*
			 * Empeche de pouvoir faire plusieurs recherches à la suite
			 * afin d'éviter un OutOfMemoryError produit par Mapsforge
			 * Quick & dirty fix
			 */
			
			searchPossible = false;
			
			//Sauvegarde des recherches récentes pour historique
			SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
	                SuggestionProvider.AUTHORITY, SuggestionProvider.MODE);
			suggestions.saveRecentQuery(query, null);
						
			//On renomme le texte de l'ActionBar
			getSupportActionBar().setTitle(getString(R.string.search_results, query));
			
			//Utilisation d'un HashSet pour éviter d'avoir plusieurs fois le même POI
			HashSet<POI> cleanList = new HashSet<POI>();
			//Ajout des POIs correspondant directement à la recherche
			cleanList.addAll(POIDb.search(query));
			//Ajout des POIs dont les catégories correspondent à la recherche
			List<Categorie> catSearch = CatDb.search(query);
			for(Categorie cat : catSearch) {
				cleanList.addAll(POIDb.getAll(cat));
			}
			//Ajout des POIs à la liste servant pour l'affichage
			listPOI = new ArrayList<POI>();
			listPOI.addAll(cleanList);
		}
		else {
			categoriesSelected = new ArrayList<Categorie>();
			categoriesSelected.addAll(CatDb.getAll());
			listPOI = POIDb.getAll();
		}

		defineMapMarker(listPOI);
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		//Démarrage du GPS en même temps que l'activité
		startGps();		
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		//On stoppe le GPS lors de la sortie de l'activité
		stopGps();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == CATEGORIE_SELECTION && resultCode == RESULT_OK) {
			List<Integer> categoriesSelectedInteger = data.getIntegerArrayListExtra("categoriesSelected");
			CategorieDAOSQLite catDAO = new CategorieDAOSQLite(this);
			categoriesSelected.clear();
			for(Integer catId : categoriesSelectedInteger) {
				categoriesSelected.add(catDAO.find(catId));
			}
			
			List<POI> listPOI = new ArrayList<POI>();
			POIDAOSQLite poiDAO = new POIDAOSQLite(this);
			for(Categorie cat : categoriesSelected) {
				listPOI.addAll(poiDAO.getAll(cat));
			}
			
			defineMapMarker(listPOI);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		menu.add(0, 1, 3, R.string.about)
			.setIcon(android.R.drawable.ic_menu_info_details);

		
		menu.add(0, 2, 2, R.string.option)
			.setIcon(android.R.drawable.ic_menu_agenda);  
		
		
		// Création des boutons de la ActionBar \\
		
		/* Bouton GPS */
		menu.add(0, 3, 0, "GPS")   
			.setIcon(android.R.drawable.ic_menu_mylocation)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			
		/* Bouton de recherche facile */
		if(searchPossible) {
			menu.add(0, 4, 0, "Recherche")
				.setIcon(android.R.drawable.ic_menu_search)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			menu.add(0, 5, 0, "Select cat")
				.setIcon(android.R.drawable.ic_menu_agenda)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}
		
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch (item.getItemId()) 
		{
			case  1:
				Intent intentAbout = new Intent(this, About.class);
				startActivity(intentAbout);
			break;
			case 2:
				Intent intentOption = new Intent(this, Option.class);
				startActivity(intentOption);
			break;
			case 3:
				centerOnLastKnowLocation();
			break;
			case 4:
				// Recherche avec search dialog !!
				onSearchRequested();
			break;
			case 5:
				Intent selectCat = new Intent(this, ViewerCategoriesChooser.class);
				ArrayList<Integer> categoriesSelectedInteger = new ArrayList<Integer>();
				for(Categorie cat : categoriesSelected) {
					categoriesSelectedInteger.add(cat.getId());
				}
				selectCat.putIntegerArrayListExtra("categoriesSelected", categoriesSelectedInteger);
				startActivityForResult(selectCat, CATEGORIE_SELECTION);
	
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_SEARCH && !searchPossible) {
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
	/**
	 * Démarre le GPS
	 */
	private void startGps() {		
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		String bestProvider = this.locationManager.getBestProvider(criteria, true);
		locationManager.requestLocationUpdates(bestProvider, 1000, 5, this);
		lastKnowLocation = locationManager.getLastKnownLocation(bestProvider);
		gpsOverlayItem.setPoint(new GeoPoint(lastKnowLocation.getLatitude(), lastKnowLocation.getLongitude()));
		synchronized (this) {
			//gpsItemizedOverlay.requestRedraw();
		}
		

	}
	
	/**
	 * Stoppe le GPS
	 */
	private void stopGps() {
		locationManager.removeUpdates(this);
	}
	
	/**
	 * Centre la carte sur la dernière position connue
	 */
	private void centerOnLastKnowLocation() {
		GeoPoint pos = new GeoPoint(lastKnowLocation.getLatitude(), lastKnowLocation.getLongitude());
		mapView.getController().setCenter(pos);
	}

	@Override
	public void onLocationChanged(Location location) {
		GeoPoint point = new GeoPoint(location.getLatitude(), location.getLongitude());
		gpsOverlayItem.setPoint(point);
		//gpsItemizedOverlay.requestRedraw();
	}

	@Override
	public void onProviderDisabled(String provider) {
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}
	
	private void defineMapMarker(List<POI> list) {
		mapView.getOverlays().clear();

		Drawable marker = getResources().getDrawable(R.drawable.marker_green);
		ArrayItemizedOverlay poiItemizedOverlay = new ArrayItemizedOverlay(marker);
		for(POI poi : list) {
			OverlayItem item = new OverlayItem(poi, poi.getNom(), poi.getDescription());
			poiItemizedOverlay.addItem(item);
		}
		
		gpsItemizedOverlay = new ArrayItemizedOverlay(getResources().getDrawable(R.drawable.ic_maps_indicator_current_position));
		gpsItemizedOverlay.addItem(gpsOverlayItem);
		
		mapView.getOverlays().add(poiItemizedOverlay);
		mapView.getOverlays().add(gpsItemizedOverlay);
		
	}
	
}