package org.campusgps.android;

import org.campusgps.metier.Categorie;

public class CategorieCheckbox {
	
	private Categorie categorie;
	private boolean selected;

	public CategorieCheckbox(Categorie categorie, boolean selected) {
		this.categorie = categorie;
		this.selected = selected;
	}
	
	public CategorieCheckbox(Categorie categorie) {
		this(categorie, false);
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
}
